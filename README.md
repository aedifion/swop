# SWOP - Simple Writing Operation Protocol

## Documentation

### Building the documentation

Install the requirements.

	pip install -r docs/requirements.txt
	
Build the docs.

	mkdocs build [--clean] [-d <site-dir>]


Or, serve them locally.

	mkdocs serve

You can also use `make`:

* `make docs-serve` to serve the documentation
* `make docs-build` to cleanly build the documentation to `build/docs`

## Python Implementation

### Running unittests

Install the requirements.

    pip install -r python/requirements.txt

Run tests.

    make python-tests
