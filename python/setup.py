import setuptools

setuptools.setup(
    name="aedifion_swop",
    version=open('VERSION').read().strip(),
    author="aedifion GmbH",
    author_email="info@aedifion.com",
    description="SWOP library",
    license='MIT License',
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/aedifion.io/libs/swop",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.6",
        "Operating System :: OS Independent",
        "Development Status :: 1 - Pre-Alpha"
    ],
    install_requires=[
        'pytz==2019.3',
        'pydantic>=1.4<2.0',
        'pybase62==0.4.1',
        'python-dateutil>=2.4',
    ]
)
