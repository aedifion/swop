# MIT License
#
# Copyright (c) 2019 aedifion.io / Libraries
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
.. module:: simple_swop_message
   :platform: Unix, Windows
   :synopsis: Demonstration of a simple swop message creation

.. moduleauthor:: Abdullah Abdullah <aabdullah@aedifion.com>
                  Jan Henrik Ziegeldorf <hziegeldorf@aedifion.com>

A simple example which demonstrates creation of new setpoint command using the python
implementation of SWOP protocol.
"""

from swop.protocol import NewSetpoint


def create_new_setpoint_command():
    # First create simple setpoint object
    new_setpoint_message = NewSetpoint(datapoint="sample_datapoint_id",
                              value=1,
                              acknowledge=True,
                              reference="sample-reference-id",
                              priority=13)

    # The message object can be converted to a json formatted string using .json()
    print(f"NewSetpoint command in json format: {new_setpoint_message.json()}")

    # Alternatively an object can be created from a valid json string using .from_json()
    another_setpoint_message = NewSetpoint.from_json(new_setpoint_message.json())
    print(f"Another NewSetpoint command created from a json string: {another_setpoint_message.json()}")


if __name__ == "__main__":
    create_new_setpoint_command()
