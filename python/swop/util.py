import json
from typing import Union, Tuple

from swop.protocol import NewSchedule, NewSetpoint, UpdateSchedule, DeleteSchedule, AcknowledgeSchedule, AcknowledgeSetpoint


def validate_message(message: Union[str, dict]) -> Tuple[bool, Union[str, Union[NewSchedule,
                                                                                NewSetpoint,
                                                                                UpdateSchedule,
                                                                                DeleteSchedule,
                                                                                AcknowledgeSchedule,
                                                                                AcknowledgeSetpoint]]]:
    """Validates a json string or a python dict written in swop.

    :param message: a json string or dict
    :type message: str, dict

    :return: returns a boolean stating validation and reason in case of failure and a python object incase of success
    :rtype: tuple
    """

    if isinstance(message, (str, bytes)):
        try:
            message_type = json.loads(message).get("type")
        except Exception:
            return False, "Invalid json string."
    else:
        try:
            message_type = message.get("type")
            message = json.dumps(message)
        except Exception:
            return False, "Non-serialzable objects in given json."

    if message_type is None or message_type not in ["ACKSPT", "ACKSCHD", "NEWSPT", "NEWSCHD", "UPSCHD", "DELSCHD"]:
        return False, "Malformed message. Reason: 'type' missing from message or not recognized."

    if message_type == "ACKSPT":
        try:
            message_object = AcknowledgeSetpoint.from_json(message)
            return True, message_object
        except Exception as e:
            return False, str(e)

    if message_type == "ACKSCHD":
        try:
            message_object = AcknowledgeSchedule.from_json(message)
            return True, message_object
        except Exception as e:
            return False, str(e)

    if message_type == "NEWSPT":
        try:
            message_object = NewSetpoint.from_json(message)
            return True, message_object
        except Exception as e:
            return False, str(e)

    if message_type == "NEWSCHD":
        try:
            message_object = NewSchedule.from_json(message)
            return True, message_object
        except Exception as e:
            return False, str(e)

    if message_type == "UPSCHD":
        try:
            message_object = UpdateSchedule.from_json(message)
            return True, message_object
        except Exception as e:
            return False, str(e)

    if message_type == "DELSCHD":
        try:
            message_object = DeleteSchedule.from_json(message)
            return True, message_object
        except Exception as e:
            return False, str(e)

    # Should never happen
    return False, "Message could not be verified."
