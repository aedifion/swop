# MIT License
#
# Copyright (c) 2019 aedifion.io / Libraries
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
.. module:: test_swop_protocol
   :platform: Unix, Windows
   :synopsis: Unit tests for python implementation of SWOP protocol.

.. moduleauthor:: Abdullah Abdullah <aabdullah@aedifion.com>
                  Jan Henrik Ziegeldorf <hziegeldorf@aedifion.com>

    .. _docs: https://docs.aedifion.io/docs/developers/writing-protocol.
    .. _license:
"""
import datetime
import json
import unittest
from typing import List

from pytz import UTC

from swop.protocol import (
    SWOP_VERSION, AcknowledgeDeleteControls, AcknowledgeResetControls, AcknowledgeSchedule, AcknowledgeSetpoint, AcknowledgeUpsertControls, Alive, BaseMessage,
    DeleteControls, DeleteSchedule, IncompatibleVersionException, NewSchedule, NewSetpoint, ResetControls, ResetValue, UpdateSchedule, UpsertControls, base62id,
    validate_swop_version)
from swop.util import validate_message


class TestSWOPProtocol(unittest.TestCase):
    """Test functions from the swop_protocol module.

    Tests defined in this class specifically test functionality related to:
      - NewSetpoint command (NEWSPT)
      - AcknowledgeSetpoint command (ACKSPT)
      - NewSchedule command (NEWSCHD)
      - UpdateSchedule command (UPSCHD)
      - DeleteSchedule command (DELSCHD)
      - AcknowledgeSchedule command (ACKSCHD)
      - UpsertControls command (UPSRTCTRL)
      - AcknowledgeUpsertControls (ACKUPSRTCTRL)
      - ResetControls (RESETCTRL)
      - AcknowledgeResetControls (ACKRESETCTRL)
      - DeleteControls (DELCTRL)
      - AcknowledgeDeleteControls (ACKDELCTRL)
      - Alive (ALIVE)
      - BaseMessage
    """
    test_reset_values: List[ResetValue] = [ResetValue(fqdn=f"test-fqdn-{i}", value=i) for i in range(4)]
    maxDiff = None

    def test_new_setpoint(self):
        """ This unittest tests creation and functionality of NewSetpoint(NEWSPT) command as defined in the SWOP protocol.
            Tested functionality:
             - Input value validation checks
             - Object to JSON and JSON to Object conversion
        """
        new_spt = NewSetpoint(datapoint="sample_datapoint_id",
                              value=1,
                              acknowledge=True,
                              reference="sample-reference-id")
        new_spt_json = new_spt.json()
        self.assertEqual(json.loads(new_spt_json), {"type": "NEWSPT",
                                                    "swop_version": SWOP_VERSION,
                                                    "datapoint": "sample_datapoint_id",
                                                    "value": 1,
                                                    "acknowledge": True,
                                                    "reference": "sample-reference-id",
                                                    "dry_run": None,
                                                    "priority": None})
        new_spt = NewSetpoint(datapoint="sample_datapoint_id",
                              value=1,
                              priority=16,
                              acknowledge=True,
                              reference="sample-reference-id")
        new_spt_json = new_spt.json()
        self.assertEqual(json.loads(new_spt_json), {"type": "NEWSPT",
                                                    "swop_version": SWOP_VERSION,
                                                    "datapoint": "sample_datapoint_id",
                                                    "value": 1,
                                                    "priority": 16,
                                                    "acknowledge": True,
                                                    "reference": "sample-reference-id",
                                                    "dry_run": None
                                                    })

        new_spt_from_json = NewSetpoint.from_json(new_spt_json)
        self.assertEqual(new_spt_from_json.json(), new_spt_json)

        # acknowledge true but no reference
        with self.assertRaises(Exception):
            _ = NewSetpoint(datapoint="sample_datapoint_id",
                            value=1,
                            acknowledge=True,
                            )

    def test_acknowledge_setpoint(self):
        """ This unittest tests creation and functionality of AcknowledgeSetpoint(ACKSPT) command as defined in the SWOP protocol.
            Tested functionality:
             - Input value validation checks
             - Object to JSON and JSON to Object conversion
        """
        # simple ack message
        ack_msg = AcknowledgeSetpoint("some-reference-id", "written")
        self.assertEqual(json.loads(ack_msg.json()), {"type": "ACKSPT",
                                                      "swop_version": SWOP_VERSION,
                                                      "reference": "some-reference-id",
                                                      "status": "written",
                                                      "message": None,
                                                      "detail": None})

        # ack message with extra parameters
        ack_spt = AcknowledgeSetpoint("some-reference-id",
                                      "written",
                                      "successfully written",
                                      {"state_before": {
                                          "priority_array": [
                                              16, "null", "null", "null", "null", "null",
                                              "null", "null", "null", "null", "null",
                                              "null", "null", 16.4, "null", "null", "null"]
                                      }})
        # json from object
        self.assertEqual(json.loads(ack_spt.json()), {"type": "ACKSPT",
                                                      "swop_version": SWOP_VERSION,
                                                      "reference": "some-reference-id",
                                                      "status": "written",
                                                      "message": "successfully written",
                                                      "detail": {"state_before": {
                                                          "priority_array": [
                                                              16, "null", "null", "null", "null", "null",
                                                              "null", "null", "null", "null", "null",
                                                              "null", "null", 16.4, "null", "null", "null"]
                                                      }}})

        # object from json
        ack_spt_from_json = AcknowledgeSetpoint.from_json(ack_spt.json())
        self.assertEqual(ack_spt_from_json.json(), ack_spt.json())

        # Passing invalid status message
        with self.assertRaises(Exception):
            _ = AcknowledgeSetpoint("some-reference-id", "written.", "some message", {"some": "object"})

        # Passing a non serializable object in details
        with self.assertRaises(Exception):
            _ = AcknowledgeSetpoint("some-reference-id", "written", "some message", {"some": object})

    def test_new_schedule(self):
        """ This unittest tests creation and functionality of NewSchedule(NEWSCHD) command as defined in the SWOP protocol.
            Tested functionality:
             - Input value validation checks
             - Object to JSON and JSON to Object conversion
        """
        date = datetime.datetime.now()
        date_to_test = UTC.localize(date).isoformat()

        new_schd = NewSchedule(datapoint="sample_datapoint",
                               setpoints=[{"start": date, "value": 1}],
                               reference="sample-reference-id",
                               name="test_schedule",
                               priority=15,
                               repeat="cron(59 10 ? * 2L 2002-2005)")
        new_schd_json = new_schd.json()
        self.assertEqual(json.loads(new_schd_json), {"type": "NEWSCHD",
                                                     "swop_version": SWOP_VERSION,
                                                     "datapoint": "sample_datapoint",
                                                     "setpoints": [{'id': base62id(f'{date_to_test}{1}'), 'start': date_to_test, 'value': 1}],
                                                     "reference": "sample-reference-id",
                                                     "name": "test_schedule",
                                                     "priority": 15,
                                                     "description": None,
                                                     "heartbeat": None,
                                                     "reset_value": None,
                                                     "repeat": "cron(59 10 ? * 2L 2002-2005)"})
        new_schd_from_json = NewSchedule.from_json(new_schd_json)
        self.assertEqual(new_schd_from_json.json(), new_schd_json)

        new_schd = NewSchedule(datapoint="sample_datapoint",
                               setpoints=[{"start": date, "value": 1}],
                               reference="sample-reference-id",
                               name="test_schedule",
                               priority=15)
        new_schd_json = new_schd.json()
        self.assertEqual(json.loads(new_schd_json), {"type": "NEWSCHD",
                                                     "swop_version": SWOP_VERSION,
                                                     "datapoint": "sample_datapoint",
                                                     "setpoints": [{'id': base62id(f'{date_to_test}{1}'), 'start': date_to_test, 'value': 1}],
                                                     "reference": "sample-reference-id",
                                                     "name": "test_schedule",
                                                     "priority": 15,
                                                     "description": None,
                                                     "heartbeat": None,
                                                     "reset_value": None,
                                                     "repeat": None})
        new_schd_from_json = NewSchedule.from_json(new_schd_json)
        self.assertEqual(new_schd_from_json.json(), new_schd_json)

        # invalid cron or rate expression
        with self.assertRaises(Exception):
            _ = NewSchedule(datapoint="sample_datapoint",
                            setpoints=[{"start": date, "value": 1}],
                            reference="sample-reference-id",
                            name="test_schedule",
                            priority=15,
                            repeat="cron(60 10 ? * L 2002-2005)")

        # missing keys or keys with incorrect values in setpoints dict
        with self.assertRaises(Exception):
            _ = NewSchedule(datapoint="sample_datapoint",
                            setpoints=[{"start": date}],
                            reference="sample-reference-id",
                            name="test_schedule",
                            priority=15)

        with self.assertRaises(Exception):
            _ = NewSchedule(datapoint="sample_datapoint",
                            setpoints=[{"value": 1}],
                            reference="sample-reference-id",
                            name="test_schedule",
                            priority=15)
        with self.assertRaises(Exception):
            _ = NewSchedule(datapoint="sample_datapoint",
                            setpoints=[{"start": "not a valid date", "value": 1}],
                            reference="sample-reference-id",
                            name="test_schedule",
                            priority=15)
        with self.assertRaises(Exception):
            _ = NewSchedule(datapoint="sample_datapoint",
                            setpoints=[{"start": date, "value": object()}],
                            reference="sample-reference-id",
                            name="test_schedule",
                            priority=15)

    def test_update_schedule(self):
        """ This unittest tests creation and functionality of UpdateSchedule(UPSCHD) command as defined in the SWOP protocol.
            Tested functionality:
             - Input value validation checks
             - Object to JSON and JSON to Object conversion
        """
        date = datetime.datetime.now()
        date_to_test = UTC.localize(date).isoformat()

        up_schd = UpdateSchedule(reference="sample-reference-id",
                                 name="updated_name",
                                 add_setpoints=[{"start": date, "value": 2}],
                                 up_setpoints=[{"id": 1, "value": 3}, {"id": 2, "value": 2}],
                                 del_setpoints=[{"id": 5}, {"id": 2}],
                                 repeat="cron(59 10 ? * 2L 2002-2005)")
        up_schd_json = up_schd.json()
        self.assertEqual(json.loads(up_schd_json), {"type": "UPSCHD",
                                                    "swop_version": SWOP_VERSION,
                                                    "add_setpoints": [{'id': base62id(f'{date_to_test}{2}'), 'start': date_to_test, 'value': 2}],
                                                    "up_setpoints": [{"id": 1, "value": 3}, {"id": 2, "value": 2}],
                                                    "del_setpoints": [{"id": 5}, {"id": 2}],
                                                    "reference": "sample-reference-id",
                                                    "name": "updated_name",
                                                    "description": None,
                                                    "heartbeat": None,
                                                    "reset_value": None,
                                                    "repeat": "cron(59 10 ? * 2L 2002-2005)"})
        up_schd_from_json = UpdateSchedule.from_json(up_schd_json)
        self.assertEqual(up_schd_from_json.json(), up_schd_json)

        up_schd = UpdateSchedule(reference="sample-reference-id",
                                 name="updated_name",
                                 add_setpoints=[{"start": date, "value": 2}],
                                 up_setpoints=[{"id": 1, "value": 3}, {"id": 2, "value": 2}],
                                 del_setpoints=[{"id": 5}, {"id": 2}])
        up_schd_json = up_schd.json()
        self.assertEqual(json.loads(up_schd_json), {"type": "UPSCHD",
                                                    "swop_version": SWOP_VERSION,
                                                    "add_setpoints": [{'id': base62id(f'{date_to_test}{2}'), 'start': date_to_test, 'value': 2}],
                                                    "up_setpoints": [{"id": 1, "value": 3}, {"id": 2, "value": 2}],
                                                    "del_setpoints": [{"id": 5}, {"id": 2}],
                                                    "reference": "sample-reference-id",
                                                    "name": "updated_name",
                                                    "description": None,
                                                    "heartbeat": None,
                                                    "reset_value": None,
                                                    "repeat": None})
        up_schd_from_json = UpdateSchedule.from_json(up_schd_json)
        self.assertEqual(up_schd_from_json.json(), up_schd_json)

        up_schd = UpdateSchedule(reference="sample-reference-id",
                                 name="updated_name",
                                 up_setpoints=[{"id": 1, "start": date}],
                                 del_setpoints=[{"id": 5}, {"id": 2}])
        up_schd_json = up_schd.json()
        self.assertEqual(json.loads(up_schd_json), {"type": "UPSCHD",
                                                    "swop_version": SWOP_VERSION,
                                                    "add_setpoints": None,
                                                    "up_setpoints": [{"id": 1, "start": date_to_test}],
                                                    "del_setpoints": [{"id": 5}, {"id": 2}],
                                                    "reference": "sample-reference-id",
                                                    "name": "updated_name",
                                                    "description": None,
                                                    "heartbeat": None,
                                                    "reset_value": None,
                                                    "repeat": None})
        up_schd_from_json = UpdateSchedule.from_json(up_schd_json)
        self.assertEqual(up_schd_from_json.json(), up_schd_json)

        # invalid cron or rate expression
        with self.assertRaises(Exception):
            _ = UpdateSchedule(reference="sample-reference-id",
                               name="updated_name",
                               add_setpoints=[{"start": date, "value": 2}],
                               up_setpoints=[{"id": 1, "value": 3}, {"id": 2, "value": 2}],
                               del_setpoints=[{"id": 5}, {"id": 2}],
                               repeat="cron(60 10 ? * L 2002-2005)")

        # missing keys or keys with incorrect values in setpoints dict
        # add_setpoints
        with self.assertRaises(Exception):
            _ = UpdateSchedule(reference="sample-reference-id",
                               name="updated_name",
                               add_setpoints=[{"start": "not a date"}],
                               up_setpoints=[{"id": 1, "value": 3}, {"id": 2, "value": 2}],
                               del_setpoints=[{"id": 5}, {"id": 2}])
        # up_setpoints
        with self.assertRaises(Exception):
            _ = UpdateSchedule(reference="sample-reference-id",
                               name="updated_name",
                               add_setpoints=[{"start": date, "value": 2}],
                               up_setpoints=[{"value": 3}, {"id": 2, "value": 2}],
                               del_setpoints=[{"id": 5}, {"id": 2}])
        # del_setpoints
        with self.assertRaises(Exception):
            _ = UpdateSchedule(reference="sample-reference-id",
                               name="updated_name",
                               add_setpoints=[{"start": date, "value": 2}],
                               up_setpoints=[{"id": 1, "value": 3}, {"id": 2, "value": 2}],
                               del_setpoints=[{"value": 2}])

    def test_delete_schedule(self):
        """ This unittest tests creation and functionality of DeleteSchedule(DELSCHD) command as defined in the SWOP protocol.
            Tested functionality:
             - Input value validation checks
             - Object to JSON and JSON to Object conversion
        """
        del_schd = DeleteSchedule(reference="sample-reference-id")
        del_schd_json = del_schd.json()
        self.assertEqual(json.loads(del_schd_json), {"type": "DELSCHD",
                                                     "swop_version": SWOP_VERSION,
                                                     "reference": "sample-reference-id"})
        del_schd_from_json = DeleteSchedule.from_json(del_schd_json)
        self.assertEqual(del_schd_from_json.json(), del_schd_json)

    def test_acknowledge_schedule(self):
        """ This unittest tests creation and functionality of AcknowledgeSchedule(DELSCHD) command as defined in the SWOP protocol.
            Tested functionality:
             - Input value validation checks
             - Object to JSON and JSON to Object conversion
        """
        date = datetime.datetime.now()
        date_to_test = UTC.localize(date).isoformat()

        # simple ack message
        ack_msg = AcknowledgeSchedule(reference="some-reference-id", status="written", time=date, detail={"reset_value": "null"})
        self.assertEqual(json.loads(ack_msg.json()), {"type": "ACKSCHD",
                                                      "swop_version": SWOP_VERSION,
                                                      "reference": "some-reference-id",
                                                      "status": "written",
                                                      "time": date_to_test,
                                                      "message": None,
                                                      "detail": {"reset_value": "null"}})
        # ack message with extra parameters
        ack_msg = AcknowledgeSchedule(reference="some-reference-id",
                                      status="written",
                                      message="successfully written",
                                      time=date,
                                      detail={"state_before": {
                                          "priority_array": [
                                              16, "null", "null", "null", "null", "null",
                                              "null", "null", "null", "null", "null",
                                              "null", "null", 16.4, "null", "null", "null"]
                                      }})
        # json from object
        self.assertEqual(json.loads(ack_msg.json()), {"type": "ACKSCHD",
                                                      "swop_version": SWOP_VERSION,
                                                      "reference": "some-reference-id",
                                                      "status": "written",
                                                      "time": date_to_test,
                                                      "message": "successfully written",
                                                      "detail": {"state_before": {
                                                          "priority_array": [
                                                              16, "null", "null", "null", "null", "null",
                                                              "null", "null", "null", "null", "null",
                                                              "null", "null", 16.4, "null", "null", "null"]
                                                      }}})
        # object from json
        ack_msg_from_json = AcknowledgeSchedule.from_json(ack_msg.json())
        self.assertEqual(ack_msg_from_json.json(), ack_msg.json())

        # Passing an invalid status
        with self.assertRaises(Exception):
            _ = AcknowledgeSchedule(reference="some-reference-id", status="written.", time=date, message="some message", detail={"some": "value"})

        # Passing a non serializable object in details
        with self.assertRaises(Exception):
            _ = AcknowledgeSchedule(reference="some-reference-id", status="written", time=date, message="some message", detail={"some": object})

        # invalid datetime
        with self.assertRaises(Exception):
            _ = AcknowledgeSchedule(reference="some-reference-id", status="written", time="not a date", message="some message", detail={"some": object})

    def test_base_message(self):
        """ Tests failure of raw base message creation"""
        with self.assertRaises(Exception):
            _ = BaseMessage(type="ACKSPT")

    def test_validate_message(self):
        new_setpoint_correct = {"type": "NEWSPT",
                                "swop_version": SWOP_VERSION,
                                "datapoint": "sample_datapoint_id",
                                "value": 1.0,
                                "acknowledge": True,
                                "reference": "sample-reference-id",
                                "dry_run": None,
                                "priority": 13}

        status, message = validate_message(new_setpoint_correct)
        self.assertEqual(status, True)
        self.assertEqual(json.loads(message.json()), new_setpoint_correct)

        # Reference missing
        new_setpoint_incorrect = {"type": "NEWSPT",
                                  "swop_version": SWOP_VERSION,
                                  "datapoint": "sample_datapoint_id",
                                  "value": 1,
                                  "acknowledge": True,
                                  "dry_run": None,
                                  "priority": 6}

        status, message = validate_message(new_setpoint_incorrect)
        self.assertEqual(status, False)

        ack_spt_correct = {"type": "ACKSPT",
                           "swop_version": SWOP_VERSION,
                           "reference": "some-reference-id",
                           "status": "written",
                           "message": "successfully written",
                           "detail": {"state_before": {
                               "priority_array": [
                                   16, "null", "null", "null", "null", "null",
                                   "null", "null", "null", "null", "null",
                                   "null", "null", 16.4, "null", "null", "null"]
                           }}}

        status, message = validate_message(ack_spt_correct)
        self.assertEqual(status, True)
        self.assertEqual(json.loads(message.json()), ack_spt_correct)

        # status has a . in it
        ack_spt_incorrect = {"type": "ACKSPT",
                             "swop_version": SWOP_VERSION,
                             "reference": "some-reference-id",
                             "status": "written.",
                             "message": "successfully written",
                             "detail": {"state_before": {
                                 "priority_array": [
                                     16, "null", "null", "null", "null", "null",
                                     "null", "null", "null", "null", "null",
                                     "null", "null", 16.4, "null", "null", "null"]
                             }}}

        status, message = validate_message(ack_spt_incorrect)
        self.assertEqual(status, False)

        new_schd_correct = {"type": "NEWSCHD",
                            "swop_version": SWOP_VERSION,
                            "datapoint": "sample_datapoint",
                            "setpoints": [{'id': 1, 'start': "2019-10-10T00:00:00", 'value': 1}],
                            "reference": "sample-reference-id",
                            "name": "test_schedule",
                            "priority": 15,
                            "description": None,
                            "heartbeat": None,
                            "reset_value": None,
                            "repeat": "cron(59 10 ? * 2L 2002-2005)"}

        status, message = validate_message(new_schd_correct)
        self.assertEqual(status, True)
        self.assertEqual(json.loads(message.json()), new_schd_correct)

        # datetime incorrect
        new_schd_incorrect = {"type": "NEWSCHD",
                              "swop_version": SWOP_VERSION,
                              "datapoint": "sample_datapoint",
                              "setpoints": [{'id': 1, 'start': "24.13.2020 18:00:00", 'value': 1}],
                              "reference": "sample-reference-id",
                              "name": "test_schedule",
                              "priority": 15,
                              "description": None,
                              "heartbeat": None,
                              "reset_value": None,
                              "repeat": "cron(59 10 ? * 2L 2002-2005)"}

        status, message = validate_message(new_schd_incorrect)
        self.assertEqual(status, False)

        up_schd_correct = {"type": "UPSCHD",
                           "swop_version": SWOP_VERSION,
                           "add_setpoints": [{'id': 8, 'start': "2020-03-24T18:00:00", 'value': 2}],
                           "up_setpoints": [{"id": 1, "value": 3}, {"id": 2, "value": 2}],
                           "del_setpoints": [{"id": 5}, {"id": 2}],
                           "reference": "sample-reference-id",
                           "name": "updated_name",
                           "description": None,
                           "heartbeat": None,
                           "reset_value": None,
                           "repeat": "cron(59 10 ? * 2L 2002-2005)"}

        status, message = validate_message(up_schd_correct)
        self.assertEqual(status, True)
        self.assertEqual(json.loads(message.json()), up_schd_correct)

        # invalid setpoints
        up_schd_incorrect = {"type": "UPSCHD",
                             "swop_version": SWOP_VERSION,
                             "add_setpoints": [{'id': 8, 'start': "24.03.2020 18:00:00"}],
                             "up_setpoints": [{"id": 1, "value": 3}, {"id": 1, "value": 2}],
                             "del_setpoints": [{"id": 5}, {"id": 2}],
                             "reference": "sample-reference-id",
                             "name": "updated_name",
                             "description": None,
                             "heartbeat": None,
                             "reset_value": None,
                             "repeat": "cron(59 10 ? * 2L 2002-2005)"}

        status, message = validate_message(up_schd_incorrect)
        self.assertEqual(status, False)

        del_schd_correct = {"type": "DELSCHD",
                            "swop_version": SWOP_VERSION,
                            "reference": "sample-reference-id"}

        status, message = validate_message(del_schd_correct)
        self.assertEqual(status, True)
        self.assertEqual(json.loads(message.json()), del_schd_correct)

        del_schd_incorrect = {"type": "DELSCHD",
                              "swop_version": SWOP_VERSION}
        status, message = validate_message(del_schd_incorrect)
        self.assertEqual(status, False)

        ack_schd_correct = {"type": "ACKSCHD",
                            "swop_version": SWOP_VERSION,
                            "reference": "some-reference-id",
                            "status": "written",
                            "time": "2020-03-24T18:00:00",
                            "message": "successfully written",
                            "detail": {"state_before": {
                                "priority_array": [
                                    16, "null", "null", "null", "null", "null",
                                    "null", "null", "null", "null", "null",
                                    "null", "null", 16.4, "null", "null", "null"]
                            }}}
        status, message = validate_message(ack_schd_correct)
        self.assertEqual(status, True)
        self.assertEqual(json.loads(message.json()), ack_schd_correct)

        # reference missing
        ack_schd_incorrect = {"type": "ACKSCHD",
                              "swop_version": SWOP_VERSION,
                              "status": "written",
                              "time": "2020-03-24T18:00:00",
                              "message": "successfully written",
                              "detail": {"state_before": {
                                  "priority_array": [
                                      16, "null", "null", "null", "null", "null",
                                      "null", "null", "null", "null", "null",
                                      "null", "null", 16.4, "null", "null", "null"]
                              }}}
        status, message = validate_message(ack_schd_incorrect)
        self.assertEqual(status, False)

    ###########################################################
    # ---------- Tests of .controls related messages ----------
    ###########################################################

    def test_alive(self):
        """ This unittest test the creation of an Alive (ALIVE) message as defined in the SWOP protocol."""
        new_alive_msg = Alive(sender_id="test_sender")
        new_alive_msg_json = new_alive_msg.json()
        self.assertEqual(json.loads(new_alive_msg_json), {"type": "ALIVE",
                                                          "swop_version": SWOP_VERSION,
                                                          "sender_id": "test_sender",
                                                          "timestamp": new_alive_msg.timestamp})

    def test_validate_swop_version__valid(self):
        """ This unittest test SWOP's version validation with a valid version."""
        validate_swop_version(swop_version="0.2")

    def test_validate_swop_version__invalid(self):
        """ This unittest test SWOP's version validation with an invalid version."""
        with self.assertRaises(IncompatibleVersionException):
            major, minor = SWOP_VERSION.split(".")
            validate_swop_version(swop_version=f"{int(major) + 1}.{minor}")

    def test_upsert_controls(self):
        """ This unittest test the creation of an UpsertControls(UPSRTCTRL) message as defined in the SWOP protocol."""
        upsert_controls_msg = UpsertControls(controls_app_id="test-controls-id",
                                             reference="test-reference-id",
                                             service_id="test-service-id",
                                             alive_timeout=300,
                                             reset_values=self.test_reset_values)
        upsert_controls_msg_json = upsert_controls_msg.json()
        reset_values = [{'fqdn': f"test-fqdn-{i}", 'priority': None, 'value': float(i)} for i in range(4)]
        self.assertEqual(json.loads(upsert_controls_msg_json), {"type": "UPSRTCTRL",
                                                                "swop_version": SWOP_VERSION,
                                                                "controls_app_id": "test-controls-id",
                                                                "max_alive_timeouts": 1,
                                                                "reference": "test-reference-id",
                                                                "service_id": "test-service-id",
                                                                "alive_timeout": 300,
                                                                "reset_values": reset_values})

    def test_acknowledge_upsert_controls(self):
        """ This unittest test the creation of an AcknowledgeUpsertControls(ACKUPSRTCTRL) message as defined in the SWOP protocol."""
        date = datetime.datetime.now()
        date_to_test = UTC.localize(date).isoformat()

        ack_upsert_controls_msg = AcknowledgeUpsertControls(controls_app_id="test-controls-id",
                                                            reference="test-reference-id",
                                                            service_id="test-service-id",
                                                            status="written",
                                                            time=date)
        ack_upsert_controls_msg_json = ack_upsert_controls_msg.json()
        self.assertEqual(json.loads(ack_upsert_controls_msg_json), {"type": "ACKUPSRTCTRL",
                                                                    "swop_version": SWOP_VERSION,
                                                                    "controls_app_id": "test-controls-id",
                                                                    "reference": "test-reference-id",
                                                                    "service_id": "test-service-id",
                                                                    "status": "written",
                                                                    "time": date_to_test,
                                                                    "message": None,
                                                                    "detail": None})

    def test_reset_controls(self):
        """ This unittest test the creation of a ResetControls(RESETCTRL) message as defined in the SWOP protocol."""
        reset_controls_msg = ResetControls(controls_app_id="test-controls-id", reference="test-reference-id")
        reset_controls_msg_json = reset_controls_msg.json()
        self.assertEqual(json.loads(reset_controls_msg_json), {"type": "RESETCTRL",
                                                               "swop_version": SWOP_VERSION,
                                                               "controls_app_id": "test-controls-id",
                                                               "reference": "test-reference-id"})

    def test_acknowledge_reset_controls(self):
        """ This unittest test the creation of an AcknowledgeResetControls(ACKRESETCTRL) message as defined in the SWOP protocol."""
        date = datetime.datetime.now()
        date_to_test = UTC.localize(date).isoformat()

        ack_reset_controls_msg = AcknowledgeResetControls(controls_app_id="test-controls-id",
                                                          reference="test-reference-id",
                                                          service_id="test-service-id",
                                                          status="written",
                                                          time=date)
        ack_reset_controls_msg_json = ack_reset_controls_msg.json()
        self.assertEqual(json.loads(ack_reset_controls_msg_json), {"type": "ACKRESETCTRL",
                                                                   "swop_version": SWOP_VERSION,
                                                                   "controls_app_id": "test-controls-id",
                                                                   "reference": "test-reference-id",
                                                                   "service_id": "test-service-id",
                                                                   "status": "written",
                                                                   "time": date_to_test,
                                                                   "message": None,
                                                                   "detail": None})

    def test_delete_controls(self):
        """ This unittest test the creation of a DeleteControls(DELCTRL) message as defined in the SWOP protocol."""
        delete_controls_msg = DeleteControls(controls_app_id="test-controls-id", reference="test-reference-id")
        delete_controls_msg_json = delete_controls_msg.json()
        self.assertEqual(json.loads(delete_controls_msg_json), {"type": "DELCTRL",
                                                                "swop_version": SWOP_VERSION,
                                                                "controls_app_id": "test-controls-id",
                                                                "reference": "test-reference-id"})

    def test_acknowledge_delete_controls(self):
        """ This unittest test the creation of a AcknowledgeDeleteControls(ACKDELCTRL) message as defined in the SWOP protocol."""
        date = datetime.datetime.now()
        date_to_test = UTC.localize(date).isoformat()

        ack_delete_controls_msg = AcknowledgeDeleteControls(controls_app_id="test-controls-id",
                                                            reference="test-reference-id",
                                                            service_id="test-service-id",
                                                            status="written",
                                                            time=date)
        ack_delete_controls_msg_json = ack_delete_controls_msg.json()
        self.assertEqual(json.loads(ack_delete_controls_msg_json), {"type": "ACKDELCTRL",
                                                                    "swop_version": SWOP_VERSION,
                                                                    "controls_app_id": "test-controls-id",
                                                                    "reference": "test-reference-id",
                                                                    "service_id": "test-service-id",
                                                                    "status": "written",
                                                                    "time": date_to_test,
                                                                    "message": None,
                                                                    "detail": None})


if __name__ == '__main__':
    unittest.main()
