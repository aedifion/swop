# MIT License
#
# Copyright (c) 2019 aedifion.io / Libraries
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
.. module:: protocol
   :platform: Unix, Windows
   :synopsis: Everything related to SWOP protocol.

.. moduleauthor:: Abdullah Abdullah <aabdullah@aedifion.com>
                  Jan Henrik Ziegeldorf <hziegeldorf@aedifion.com>

This module contains classes and utils related to SWOP.
For more detailed outline of the protocol.
    .. _docs: https://docs.aedifion.io/docs/developers/writing-protocol.
"""

import datetime
import enum
import hashlib
import json
import re
import time
import uuid
from abc import ABC
from typing import Any, Dict, List, Optional, Union

import base62
from dateutil import parser
from pydantic import BaseModel, Field, validator
from pytz import UTC


class IncompatibleVersionException(Exception):
    pass


def cron_rate_expression_validator(expression: str) -> bool:
    """ Validates a given expression according to AWS cron or rate expressions specifications.

    .. _reference: https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html

    :param expression: a cron or rate expression as string

    :returns: A boolean True if a valid expression and False if invalid.
    :rtype: bool

    **Examples:**

    .. testsetup::

        from aedifion_swop.protocol import cron_rate_expression_validator

    >>> cron_rate_expression_validator("cron(15 10 ? * 6L 2002-2005)")
    True
    >>> cron_rate_expression_validator("cron(60 10 ? * L 2002-2005)")
    False
    >>> cron_rate_expression_validator("cron(0 9 ? * 2#1 *)")
    True
    >>> cron_rate_expression_validator("rate(1 day)")
    True
    >>> cron_rate_expression_validator("rate(19 days)")
    True
    >>> cron_rate_expression_validator("rate(1 days)")
    False
    """
    validation_expression = r"^(rate\(((1 (hour|minute|day))|((?!1)\d+ (hours|minutes|days)))\))|" \
                            r"(cron\(\s*($|#|\w+\s*=|(\?|\*|(?:[0-5]?\d)(?:(?:-|/|,)(?:[0-5]?\d))?(?:,(?:[0-5]?\d)" \
                            r"(?:(?:-|/|,)(?:[0-5]?\d))?)*)\s+(\?|\*|(?:[01]?\d|2[0-3])(?:(?:-|/|,)(?:[01]?\d|2[0-3]))" \
                            r"?(?:,(?:[01]?\d|2[0-3])(?:(?:-|/|,)(?:[01]?\d|2[0-3]))?)*)\s+(\?|\*|(?:0?[1-9]|[12]\d|3[01])" \
                            r"(?:(?:-|/|,)(?:0?[1-9]|[12]\d|3[01]))?(?:,(?:0?[1-9]|[12]\d|3[01])(?:(?:-|/|,)(?:0?[1-9]|[12]\d|3[01]))?)*)" \
                            r"\s+(\?|\*|(?:[1-9]|1[012])(?:(?:-|/|,)(?:[1-9]|1[012]))?(?:L|W|#)?(?:[1-9]|1[012])?(?:,(?:[1-9]|1[012])" \
                            r"(?:(?:-|/|,)(?:[1-9]|1[012]))?(?:L|W|#)?(?:[1-9]|1[012])?)*|\?|\*|(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)" \
                            r"(?:(?:-)(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?(?:,(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC)" \
                            r"(?:(?:-)(?:JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|OCT|NOV|DEC))?)*)\s+(\?|\*|(?:[1-7])(?:(?:-|/|,|#)(?:[1-7]))?(?:L)?" \
                            r"(?:,(?:[1-7])(?:(?:-|/|,|#)(?:[1-7]))?(?:L)?)*|\?|\*|(?:MON|TUE|WED|THU|FRI|SAT|SUN)(?:(?:-)(?:MON|TUE|WED|THU|FRI|SAT|SUN))" \
                            r"?(?:,(?:MON|TUE|WED|THU|FRI|SAT|SUN)(?:(?:-)(?:MON|TUE|WED|THU|FRI|SAT|SUN))?)*)(|\s)+(\?|\*|(?:|\d{4})(?:(?:-|/|,)(?:|\d{4}))" \
                            r"?(?:,(?:|\d{4})(?:(?:-|/|,)(?:|\d{4}))?)*))\))$"
    if re.fullmatch(validation_expression, expression):
        return True
    else:
        return False


def base62id(s: str, length: int = 8):
    """ Generates base62 hash ids from the input string.
    Hash ids are used to reduce a long input string to a short one.
    Args:
        s (str): The string for which to build a hash id.
        length (integer): The length of the hash id.

    Returns:
        string. The hash identifier of the desired length.

    **Examples:**

    .. testsetup::

        from aedifion_swop.protocol import base62id

    >>> base62id("bacnet100-4120-CO2", length=6)
    'LqKtOV'
    >>> base62id("bacnet100-4120-CO2")
    'LqKtOVC4'
    """
    if length is None:
        length = 8
    return base62.encodebytes(hashlib.sha1(s.encode('utf-8')).digest())[:length]


def is_json_serializable(obj):
    """ Convenience function to check if values are JSON serializable"""
    try:
        json.dumps(obj)
        return True
    except Exception:
        return False


SWOP_VERSION = "0.2"


def validate_swop_version(**kwargs):
    if "swop_version" in kwargs:
        version = str(kwargs["swop_version"])
        try:
            major_given, minor_given = version.split(".")
            major_current, minor_current = SWOP_VERSION.split(".")
            if major_given > major_current or minor_given > minor_current:
                raise IncompatibleVersionException(f"Received object has a different SWOP protocol version '{kwargs['swop_version']}' "
                                                   f"than the current version '{SWOP_VERSION}'.")
        except Exception:
            raise IncompatibleVersionException(f"Received object has a different SWOP protocol version '{kwargs['swop_version']}' "
                                               f"than the current version '{SWOP_VERSION}'.")


class OperationStatus(str, enum.Enum):
    INITIALIZED = "initialized"
    REQUESTED = "requested"
    WRITTEN = "written"
    STOPPING = "stopping"
    TERMINATED = "terminated"
    FAILED = "failed"
    ACTIVE = "active"


class BaseMessage(BaseModel, ABC):
    """ Abstract class for all message objects defined in this module."""
    type: str = Field(..., title="SWOP message type", description="The type of swop message.")
    swop_version: str = Field(SWOP_VERSION, title="SWOP Protocol version", description="Version of the used SWOP protocol.")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @classmethod
    def from_json(cls, json_string):
        return cls(**json.loads(json_string))

    def __new__(cls, *args, **kwargs):
        if cls is BaseMessage:
            raise TypeError("Can't instantiate abstract class {}".format(BaseMessage.__name__))
        return super(BaseMessage, cls).__new__(cls)


class NewSetpoint(BaseMessage):
    """The NewSetpoint message `NEWSPT` contains all information required to write a setpoint.

        :param datapoint: The identifier of the datapoint to write to.
        :param value: value that should be written.
        :param priority: if required by the protocol, The priority to write at.
        :param acknowledge: Whether this operation must be acknowledged by the receiver.
        :param dry_run: Whether to perform a dry run of the requested action or not.
        :param reference: Reference for this operation on the API side.

        :return: an instance of this class
        :rtype: object
    """

    datapoint: str = Field(..., title="Unique datapoint identifier", description="The identifier of the datapoint to write to.")
    value: Union[float, int, str, bool] = Field(..., title="Value to be written", description="The value to write. "
                                                                                              "The required type depends on the target datapoint.")
    priority: Optional[int] = Field(None, title="Priority level for this setpoint", description="The priority to write at.")
    acknowledge: Optional[bool] = Field(None, title="Acknowledge required", description="Whether this operation must be acknowledged by the receiver.")
    dry_run: Optional[bool] = Field(None, title="Dry run", description="Whether to perform a dry run of the requested action or not.")
    reference: Optional[str] = Field(None, title="A unique reference id", description="Reference for this operation on the API side.")

    def __init__(self,
                 datapoint: str,
                 value: Union[float, int, str, bool],
                 priority: Optional[int] = None,
                 acknowledge: Optional[bool] = None,
                 dry_run: Optional[bool] = None,
                 reference: Optional[str] = None,
                 **kwargs):

        validate_swop_version(**kwargs)

        super().__init__(type="NEWSPT",
                         datapoint=datapoint,
                         value=value,
                         priority=priority,
                         acknowledge=acknowledge,
                         dry_run=dry_run,
                         reference=reference
                         )

    @validator('reference', always=True)
    def validate_reference(cls, reference, values):
        if values['acknowledge']:
            if reference is None:
                raise ValueError("A reference must be given if acknowledge is requested.")
        return reference


class AcknowledgeSetpoint(BaseMessage):
    """The Acknowledge Setpoint message `ACKSPT` conveys an acknowledgement of success or error
       from the receiver of a write operation to the issuer.

        :param reference: Reference for the write operation.
        :param status: The status of the setpoint write operation. Valid status messages are defined in ~.OperationStatus.
        :param message: A human readable success or error message.
        :param detail: Additional custom information.

        :return: an instance of this class
        :rtype: object
    """
    reference: str = Field(..., title="A unique reference id", description="Reference for the write operation.")
    status: OperationStatus = Field(..., title="Status of write operation", description="The status of the setpoint write operation.")
    message: Optional[str] = Field(None, title="Message", description="A human readable success or error message.")
    detail: Optional[Dict[str, Any]] = Field(None, title="Additional information", description="Additional custom information.")

    def __init__(self, reference: str, status: str, message: Optional[str] = None, detail: Optional[Dict[str, Any]] = None, *args, **kwargs):

        validate_swop_version(**kwargs)

        super().__init__(type="ACKSPT",
                         reference=reference,
                         status=status,
                         message=message,
                         detail=detail)

    @validator('detail')
    def validate_detail(cls, detail):
        if detail:
            if not is_json_serializable(detail):
                raise ValueError("Detail must be json serializable.")
        return detail


class NewSchedule(BaseMessage):
    """The NewSchedule message `NEWSCHD` contains all information required to create a schedule.

        :param datapoint: The identifier of the datapoint to write to.
        :param setpoints: A list of setpoints. Each setpoint is defined by a unique `id`, a `start` time, and a `value`.
        :param reference: A human readable name for this schedule.
        :param name: A human readable name for this schedule.
        :param priority: The priority to write at, if supported by the automation protocol.
        :param description: A human readable description of this schedule.
        :param heartbeat: duration in seconds after which the schedule deletes itself if no heartbeat is received
        :param reset_value: the value to write when the schedule finishes or fails. Uses existing value before schedule, if not provided
        :param repeat: repeat interval of this schedule as a cron or rate expression.

        :return: an instance of this class
        :rtype: object
    """
    datapoint: str = Field(..., title="Unique datapoint identifier", description="The identifier of the datapoint to write to.")
    setpoints: List[Dict[str, Any]] = Field(..., title="A list of setpoints", description="A list of setpoints. Each setpoint is defined by a unique `id`, "
                                                                                          "a `start` time, and a `value`.")
    reference: str = Field(..., title="A unique reference id", description="Unique reference for this operation.")
    name: str = Field(..., title="Name of the schedule", description="A human readable name for this schedule.")
    priority: Optional[int] = Field(None, title="Priority level for this schedule", description="The priority to write at, if supported by the "
                                                                                                "automation protocol.", gt=0)
    description: Optional[str] = Field(None, title="Description of the schedule", description="A human readable description of this schedule.")
    heartbeat: Optional[int] = Field(None, title="Heartbeat of the schedule", description="Duration in seconds after which the schedule deletes itself if no "
                                                                                          "heartbeat is received.", gt=0)
    reset_value: Optional[Union[int, float, bool, str]] = Field(..., title="Reset value", description="The value to write when the schedule finishes or "
                                                                                                      "fails. Uses existing value before schedule, "
                                                                                                      "if not provided.")
    repeat: Optional[str] = Field(None, title="Repeat interval", description="Repeat interval of this schedule as a cron or rate expression.")

    def __init__(self,
                 datapoint: str,
                 setpoints: List[Dict[str, Any]],
                 reference: str,
                 name: str,
                 priority: Optional[int] = None,
                 description: Optional[str] = None,
                 heartbeat: Optional[int] = None,
                 reset_value: Optional[Union[int, float, bool, str]] = None,
                 repeat: Optional[str] = None,
                 *args,
                 **kwargs):

        validate_swop_version()

        super().__init__(type="NEWSCHD",
                         datapoint=datapoint,
                         setpoints=setpoints,
                         reference=reference,
                         name=name,
                         priority=priority,
                         description=description,
                         heartbeat=heartbeat,
                         reset_value=reset_value,
                         repeat=repeat,
                         )

    @validator('setpoints', whole=True)
    def validate_setpoints(cls, setpoints):
        for setpoint in setpoints:
            if not isinstance(setpoint, dict) or any(req_key not in setpoint.keys() for req_key in ["start", "value"]):
                raise TypeError("Setpoints must be a dict containing keys: `start` with a datetime and `value` with a value to write.")
            if not any(isinstance(setpoint["value"], t) for t in [int, str, bool, float]):
                raise ValueError(f'`value` has unsupported data type: {type(setpoint["value"])}.')
            dt = setpoint["start"]
            if isinstance(dt, datetime.datetime):
                if dt.tzinfo is None:
                    dt = UTC.localize(dt)
                setpoint["start"] = dt.isoformat()
            elif isinstance(dt, str):
                try:
                    dt = parser.parse(dt)
                    if dt.tzinfo is None:
                        UTC.localize(dt)
                    setpoint["start"] = dt.isoformat()
                except ValueError:
                    raise
            else:
                raise ValueError("`start` can be either a datetime string or a datetime object only.")
            if "id" not in setpoint.keys():
                setpoint["id"] = base62id(f'{setpoint["start"]}{setpoint["value"]}')
        setpoints_allowed_keys_only = []
        for setpoint in setpoints:
            allowed = {}
            for key in ["id", "start", "value"]:
                if setpoint.get(key):
                    allowed[key] = setpoint.get(key)
            setpoints_allowed_keys_only.append(allowed)
        return setpoints_allowed_keys_only

    @validator('repeat')
    def validate_repeat(cls, repeat):
        if repeat:
            if not cron_rate_expression_validator(repeat):
                raise ValueError(f"{repeat} could not be parsed as a valid cron or rate expression.")
        return repeat


class UpdateSchedule(BaseMessage):
    """The UpdateSchedule message `UPSCHD` is used bidirectionally between issuer and receiver to exchange updates to an active schedule.

        :param add_setpoints: A list of dicts with `start` as datetime string or datetime object and `value` as value that should be written.
        :param up_setpoints: A list of dicts with `id` of the setpoint to update and `value` and/or `start` to update the respective parameters.
        :param del_setpoints: A list of dicts with `id` of the setpoint to delete.
        :param reference: Unique reference of the schedule to update.
        :param name: Updated name for the referenced schedule.
        :param description: Updated description for the referenced schedule.
        :param heartbeat: 	Updated heartbeat for the referenced schedule.
        :param reset_value: The value to write when the schedule finishes or fails.
        :param repeat: repeat interval of this schedule as a cron or rate expression.

        :return: an instance of this class
        :rtype: object
        """
    reference: str = Field(..., title="A unique reference id", description="Unique reference of the schedule to update.")
    name: Optional[str] = Field(None, title="Name of the schedule", description="Updated name for the referenced schedule.")
    add_setpoints: Optional[List[Dict[str, Any]]] = Field(None,
                                                          title="A list of setpoints to add",
                                                          description="A list of new setpoints to add to the referenced schedule.")
    up_setpoints: Optional[List[Dict[str, Any]]] = Field(None,
                                                         title="A list of setpoints to update",
                                                         description="A list of existing setpoints in the reference schedule to update.")
    del_setpoints: Optional[List[Dict[str, Any]]] = Field(None,
                                                          title="A list of setpoints to delete",
                                                          description="A list of existing setpoints to delete from the referenced schedule.")
    description: Optional[str] = Field(None, title="Description of the schedule", description="Updated description for the referenced schedule.")
    heartbeat: Optional[int] = Field(None, title="Heartbeat of the schedule", description="	Updated heartbeat for the referenced schedule.", gt=0)
    reset_value: Optional[Union[int, float, bool, str]] = Field(None, title="Reset value", description="The value to write when the schedule "
                                                                                                       "finishes or fails.")
    repeat: Optional[str] = Field(None, title="Repeat interval", description="Repeat interval of this schedule as a cron or rate expression.")

    def __init__(self,
                 reference: str,
                 name: Optional[str] = None,
                 add_setpoints: Optional[List[Dict[str, Any]]] = None,
                 up_setpoints: Optional[List[Dict[str, Any]]] = None,
                 del_setpoints: Optional[List[Dict[str, Any]]] = None,
                 description: Optional[str] = None,
                 heartbeat: Optional[int] = None,
                 reset_value: Optional[Union[int, float, bool, str]] = None,
                 repeat: Optional[str] = None,
                 *args,
                 **kwargs):

        validate_swop_version(**kwargs)

        super().__init__(type="UPSCHD",
                         reference=reference,
                         name=name,
                         add_setpoints=add_setpoints,
                         up_setpoints=up_setpoints,
                         del_setpoints=del_setpoints,
                         description=description,
                         heartbeat=heartbeat,
                         reset_value=reset_value,
                         repeat=repeat,
                         )

    @validator('add_setpoints', whole=True)
    def validate_add_setpoints(cls, setpoints):
        if setpoints:
            for setpoint in setpoints:
                if not isinstance(setpoint, dict) or any(req_key not in setpoint.keys() for req_key in ["start", "value"]):
                    raise TypeError("Setpoints must be a dict containing keys: `start` with a datetime and `value` with a value to write.")
                if not any(isinstance(setpoint["value"], t) for t in [int, str, bool, float]):
                    raise ValueError(f'`value` has unsupported data type: {type(setpoint["value"])}.')
                dt = setpoint["start"]
                if isinstance(dt, datetime.datetime):
                    if dt.tzinfo is None:
                        dt = UTC.localize(dt)
                    setpoint["start"] = dt.isoformat()
                elif isinstance(dt, str):
                    try:
                        dt = parser.parse(dt)
                        if dt.tzinfo is None:
                            UTC.localize(dt)
                        setpoint["start"] = dt.isoformat()
                    except ValueError:
                        raise
                else:
                    raise ValueError("`start` can be either a datetime string or a datetime object only.")
                if "id" not in setpoint.keys():
                    setpoint["id"] = base62id(f'{setpoint["start"]}{setpoint["value"]}')
            setpoints_allowed_keys_only = []
            for setpoint in setpoints:
                allowed = {}
                for key in ["id", "start", "value"]:
                    if setpoint.get(key):
                        allowed[key] = setpoint.get(key)
                setpoints_allowed_keys_only.append(allowed)
            return setpoints_allowed_keys_only
        return None

    @validator('up_setpoints', whole=True)
    def validate_up_setpoints(cls, setpoints):
        if setpoints:
            for setpoint in setpoints:
                if not isinstance(setpoint, dict) or any(req_key not in setpoint.keys() for req_key in ["id"]):
                    raise TypeError("Setpoints must be a dict containing keys: `id` of the setpoint to update and `value` with a value to write.")
                if "value" in setpoint:
                    if not any(isinstance(setpoint["value"], t) for t in [int, str, bool, float]):
                        raise ValueError(f'`value` has unsupported data type: {type(setpoint["value"])}.')
                if "start" in setpoint:
                    dt = setpoint["start"]
                    if isinstance(dt, datetime.datetime):
                        if dt.tzinfo is None:
                            dt = UTC.localize(dt)
                        setpoint["start"] = dt.isoformat()
                    elif isinstance(dt, str):
                        try:
                            dt = parser.parse(dt)
                            if dt.tzinfo is None:
                                UTC.localize(dt)
                            setpoint["start"] = dt.isoformat()
                        except ValueError:
                            raise
                    else:
                        raise ValueError("`start` can be either a datetime string or a datetime object only.")
            setpoints_allowed_keys_only = []
            for setpoint in setpoints:
                allowed = {}
                for key in ["id", "start", "value"]:
                    if setpoint.get(key):
                        allowed[key] = setpoint.get(key)
                setpoints_allowed_keys_only.append(allowed)
            return setpoints_allowed_keys_only
        return None

    @validator('del_setpoints', whole=True)
    def validate_del_setpoints(cls, setpoints):
        if setpoints:
            for setpoint in setpoints:
                if not isinstance(setpoint, dict) or any(req_key not in setpoint.keys() for req_key in ["id"]):
                    raise TypeError("Setpoints must be a dict containing key: `id` of the setpoint to delete.")
            setpoints_allowed_keys_only = []
            for setpoint in setpoints:
                allowed = {}
                for key in ["id"]:
                    if setpoint.get(key):
                        allowed[key] = setpoint.get(key)
                setpoints_allowed_keys_only.append(allowed)
            return setpoints_allowed_keys_only
        return None

    @validator('repeat')
    def validate_repeat(cls, repeat):
        if repeat:
            if not cron_rate_expression_validator(repeat):
                raise ValueError(f"{repeat} could not be parsed as a valid cron or rate expression.")
        return repeat


class DeleteSchedule(BaseMessage):
    """The DeleteSchedule message `DELSCHD` is used to stop an active schedule.

        :param reference: Unique reference of the schedule to delete.

        :return: an instance of this class
        :rtype: object
    """
    reference: str = Field(..., title="A unique reference", description="Reference id for this message.")

    def __init__(self,
                 reference: str,
                 *args,
                 **kwargs):
        validate_swop_version(**kwargs)

        super().__init__(type="DELSCHD",
                         reference=reference,
                         )


class AcknowledgeSchedule(BaseMessage):
    """The Acknowledge Setpoint message `ACKSCHD` conveys an acknowledgement of success or error
       from the receiver of a write operation to the issuer.

        :param reference: Reference for the write operation.
        :param status: The status of the schedule write operation. Valid status messages are defined in ~.OperationStatus.
        :param message: A human readable success or error message.
        :param detail: Additional custom information.
        :param time: Time of the acknowledged event on the receiver side.

        :return: an instance of this class
        :rtype: object
    """
    reference: str = Field(..., title="A unique reference", description="Reference for the write operation.")
    status: OperationStatus = Field(..., title="Status of write operation", description="The status of the schedule write operation.")
    time: Union[str, datetime.datetime] = Field(..., title="Time of acknowledgement", description="Time of the acknowledged event on the receiver side.")
    message: Optional[str] = Field(None, title="Message", description="A human readable success or error message.")
    detail: Optional[Dict[str, Any]] = Field(None, title="Additional details", description="Additional custom information.")

    def __init__(self, reference: str, status: str, time: Union[str, datetime.datetime],
                 message: Optional[str] = None, detail: Optional[Dict[str, Any]] = None,
                 reset_value: Optional[Union[int, float, bool, str]] = None, *args, **kwargs):

        validate_swop_version(**kwargs)

        super().__init__(type="ACKSCHD",
                         reference=reference,
                         status=status,
                         time=time,
                         message=message,
                         reset_value=reset_value,
                         detail=detail)

    @validator('time')
    def validate_time(cls, time):
        if isinstance(time, datetime.datetime):
            if time.tzinfo is None:
                time = UTC.localize(time)
            time = time.isoformat()
        elif isinstance(time, str):
            try:
                time = parser.parse(time)
                if time.tzinfo is None:
                    UTC.localize(time)
                time = time.isoformat()
            except ValueError:
                raise
        else:
            raise ValueError("`time` can be either a datetime string or a datetime object only.")
        return time

    @validator('detail')
    def validate_detail(cls, detail):
        if detail:
            if not is_json_serializable(detail):
                raise ValueError("Detail must be json serializable.")
        return detail


###########################################################
# -------------- .controls related messages ---------------
###########################################################


class Alive(BaseMessage):
    """ The Alive message `ALIVE` contains information of a dedicated receiver and a timestamp.
        It will be used to check if dependent services are online/alive and can consume further SWOP messages.

        :param receiver_id: Id of the receiver of this message.
        :param timestamp: Timestamp in nanoseconds when this message was send.

        :return: an instance of this class
        :rtype: object
    """

    sender_id: str = Field(..., title="Sender ID", description="The identifier of the sender of this message.")
    timestamp: int = Field(..., title="Alive timestamp.", description="The timestamp when this message was send in nanoseconds.")

    def __init__(self, sender_id: str, timestamp: Optional[int] = None, **kwargs):
        validate_swop_version(**kwargs)
        timestamp = timestamp or time.time_ns()
        super().__init__(type="ALIVE", sender_id=sender_id, timestamp=timestamp)


class AcknowledgeControls(BaseMessage):
    """ Base class for acknowledging controls related operations.

        :param controls_app_id: Id of the corresponding controls app.
        :param service_id: Id of the service acknowledging the controls message, i.e., the sender id.
        :param status: The status of the controls operation. Valid status messages are defined in `OperationStatus`.
        :param message: A human readable success or error message.
        :param detail: Additional custom information.
        :param time: Time of the acknowledged event on the receiver side.
        :param reference: Unique reference for this message to ensure integrity.

        :return: an instance of this class
        :rtype: object
    """

    controls_app_id: str = Field(..., title="Controls App ID", description="Identifier of a controls app.")
    service_id: str = Field(..., title="Service ID", description="Identifier of the service acknowledging the UPSRTCTRL message.")
    status: OperationStatus = Field(..., title="Status of the controls operation", description="The status of the controls operation.")
    time: Union[str, datetime.datetime] = Field(..., title="Time of acknowledgement", description="Time of the acknowledged event on the receiver side.")
    message: Optional[str] = Field(None, title="Message", description="A human readable success or error message.")
    detail: Optional[Dict[str, Any]] = Field(None, title="Additional details", description="Additional custom information.")
    reference: Optional[str] = Field(None, title="A unique reference id", description="Unique reference for this message to ensure integrity.")

    def __init__(self,
                 type: str,
                 controls_app_id: str,
                 service_id: str,
                 status: str,
                 time: Union[str, datetime.datetime],
                 message: Optional[str] = None,
                 detail: Optional[Dict[str, Any]] = None,
                 reference: Optional[str] = None):
        super().__init__(type=type,
                         controls_app_id=controls_app_id,
                         service_id=service_id,
                         status=status,
                         time=time,
                         message=message,
                         detail=detail,
                         reference=reference)

    @validator('time')
    def validate_time(cls, time):
        if isinstance(time, datetime.datetime):
            if time.tzinfo is None:
                time = UTC.localize(time)
            time = time.isoformat()
        elif isinstance(time, str):
            try:
                time = parser.parse(time)
                if time.tzinfo is None:
                    UTC.localize(time)
                time = time.isoformat()
            except ValueError:
                raise
        else:
            raise ValueError("`time` can be either a datetime string or a datetime object only.")
        return time

    @validator('detail')
    def validate_detail(cls, detail):
        if detail and not is_json_serializable(detail):
            raise ValueError("Detail must be json serializable.")
        return detail


class ResetValue(BaseModel):
    """ This class represents a reset value for controls related datapoints, containing an fqdn of a
        writable datapoint and its actual reset value.

        :param fqdn: Fully qualified datapoint name of a writable datapoint.
        :param reset_value: Reset value of the writable datapoint.
        :param priority: The priority to write at, if supported by the automation protocol.

        :return: an instance of this class
        :rtype: object
    """
    fqdn: str = Field(..., title="Fully qualified datapoint name", description="The fqdn of a writable datapoint.")
    value: Union[float, int, str, bool] = Field(..., title="Reset value", description="The reset value of a writable datapoint.")
    priority: Optional[int] = Field(None, title="Priority level for this value", description="The priority to write at, if supported by the "
                                                                                             "automation protocol.", gt=0)


class UpsertControls(BaseMessage):
    """ The UpsertControls message `UPSRTCTRL` contains information and a context for controls apps.
        It will be used to either update or insert a new controls app context in a service.

        :param controls_app_id: Id of the corresponding controls app.
        :param service_id: Id of the service running the controls app.
        :param reset_values: A list of reset values.
        :param alive_timeout: Timeout after which a controls app is reset, if no ALIVE message has been received by then.
        :param reference: Unique reference for this message to ensure integrity.

        :return: an instance of this class
        :rtype: object
    """

    controls_app_id: str = Field(..., title="Controls App ID", description="Identifier of a controls app.")
    service_id: str = Field(..., title="Service ID", description="Identifier of the service running the controls app.")
    reset_values: List[ResetValue] = Field(..., title="Reset values", description="List of reset values for a controls app.")
    alive_timeout: int = Field(300,
                               title="Alive timeout",
                               description="Timeout in seconds after which a controls app is reset, if no ALIVE message has been received by then.",
                               gt=0)
    max_alive_timeouts: int = Field(1,
                                    title="Maximal ALIVE timeouts",
                                    description="Amount of accepted timeouts before a controls app will actually be reset.",
                                    ge=0)
    reference: Optional[str] = Field(None, title="A unique reference id", description="Unique reference for this message to ensure integrity.")

    def __init__(self,
                 controls_app_id: str,
                 service_id: str,
                 reset_values: List[ResetValue],
                 alive_timeout: Optional[int] = None,
                 max_alive_timeouts: Optional[int] = None,
                 reference: Optional[str] = None,
                 **kwargs):
        validate_swop_version(**kwargs)
        reference = reference or str(uuid.uuid4())
        super().__init__(type="UPSRTCTRL",
                         controls_app_id=controls_app_id,
                         service_id=service_id,
                         reset_values=reset_values,
                         alive_timeout=alive_timeout or 300,
                         max_alive_timeouts=max_alive_timeouts or 1,
                         reference=reference)


class AcknowledgeUpsertControls(AcknowledgeControls):
    """ The Acknowledge Upsert Controls message `ACKUPSRTCTRL` conveys an acknowledgement of success or error
       from the receiver of a controls app context operation to the issuer.

        :return: an instance of this class
        :rtype: object
    """

    def __init__(self,
                 controls_app_id: str,
                 service_id: str,
                 status: str,
                 time: Union[str, datetime.datetime],
                 message: Optional[str] = None,
                 detail: Optional[Dict[str, Any]] = None,
                 reference: Optional[str] = None,
                 **kwargs):
        validate_swop_version(**kwargs)
        super().__init__(type="ACKUPSRTCTRL",
                         controls_app_id=controls_app_id,
                         service_id=service_id,
                         status=status,
                         time=time,
                         message=message,
                         detail=detail,
                         reference=reference)


class ResetControls(BaseMessage):
    """ The Reset Controls message `RESETCTRL` triggers a service to write reset values of a controls app to a BAS.

        :param controls_app_id: Id of the corresponding controls app
        :param reference: Unique reference for this message to ensure integrity.

        :return: an instance of this class
        :rtype: object
    """
    controls_app_id: str = Field(..., title="Controls App ID", description="Identifier of a controls app.")
    reference: Optional[str] = Field(None, title="A unique reference id", description="Unique reference for this message to ensure integrity.")

    def __init__(self, controls_app_id: str, reference: Optional[str] = None, **kwargs):
        validate_swop_version(**kwargs)
        reference = reference or str(uuid.uuid4())
        super().__init__(type="RESETCTRL", controls_app_id=controls_app_id, reference=reference)


class AcknowledgeResetControls(AcknowledgeControls):
    """ The Acknowledge Reset Controls message `ACKRESETCTRL` conveys an acknowledgement of success or error
       from the receiver of a reset controls operation to the issuer.

        :return: an instance of this class
        :rtype: object
    """

    def __init__(self,
                 controls_app_id: str,
                 service_id: str,
                 status: str,
                 time: Union[str, datetime.datetime],
                 message: Optional[str] = None,
                 detail: Optional[Dict[str, Any]] = None,
                 reference: Optional[str] = None,
                 **kwargs):
        validate_swop_version(**kwargs)
        super().__init__(type="ACKRESETCTRL",
                         controls_app_id=controls_app_id,
                         service_id=service_id,
                         status=status,
                         time=time,
                         message=message,
                         detail=detail,
                         reference=reference)


class DeleteControls(BaseMessage):
    """ The Delete Controls message `DELCTRL` tells a service to delete its context information of a controls app.

        :param controls_app_id: Id of the corresponding controls app.
        :param reference: Unique reference for this message to ensure integrity.

        :return: an instance of this class
        :rtype: object
    """
    controls_app_id: str = Field(..., title="Controls App ID", description="Identifier of a controls app.")
    reference: Optional[str] = Field(None, title="A unique reference id", description="Unique reference for this message to ensure integrity.")

    def __init__(self, controls_app_id: str, reference: Optional[str] = None, **kwargs):
        validate_swop_version(**kwargs)
        reference = reference or str(uuid.uuid4())
        super().__init__(type="DELCTRL", controls_app_id=controls_app_id, reference=reference)


class AcknowledgeDeleteControls(AcknowledgeControls):
    """ The Acknowledge Delete Controls message `ACKDELCTRL` conveys an acknowledgement of success or error
       from the receiver of a delete controls operation to the issuer.

        :return: an instance of this class
        :rtype: object
    """

    def __init__(self,
                 controls_app_id: str,
                 service_id: str,
                 status: str,
                 time: Union[str, datetime.datetime],
                 message: Optional[str] = None,
                 detail: Optional[Dict[str, Any]] = None,
                 reference: Optional[str] = None,
                 **kwargs):
        validate_swop_version(**kwargs)
        super().__init__(type="ACKDELCTRL",
                         controls_app_id=controls_app_id,
                         service_id=service_id,
                         status=status,
                         time=time,
                         message=message,
                         detail=detail,
                         reference=reference)
