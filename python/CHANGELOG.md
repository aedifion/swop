# Version history

## 1.1.0
- Changed official SWOP version to "0.2"
- Added .controls related messages:
  - Alive(ALIVE)
  - UpsertControls(UPSRTCTRL)
  - AcknowledgeUpsertControls(ACKUPSRTCTRL)
  - ResetControls(RESETCTRL)
  - AcknowledgeResetControls(ACKRESETCTRL)
  - DeleteControls(DELCTRL)
  - AcknowledgeDeleteControls(ACKDELCTRL)
- Added .controls related unit tests

## 1.0.1
- SWOP version is now evaluated as string

## 1.0

- Updated to pydantic version 1.4
- Removed automation protocol from implementation
- Added a util function for message validation
- Removed `reset_value` field from AcknowledgeSchedule message

## 0.1

- Initial version of SWOP protocol as python implementation
- Introduces:
  - NewSetpoint message (NEWSPT)
  - AcknowledgeSetpoint message (ACKSPT)
  - NewSchedule message (NEWSCHD)
  - UpdateSchedule message (UPSCHD)
  - DeleteSchedule message (DELSCHD)
  - AcknowledgeSchedule message (ACKSCHD)
- Includes basic unit tests
