## Social media

Follow aedifion GmbH on [LinkedIn](https://www.linkedin.com/company/aedifion/), [Twitter](https://twitter.com/aedifion), [Facebook](https://www.facebook.com/aedifion/), and/or [Medium](https://medium.com/@aedifion) to keep up with new features and products, speaking engagements, opinion articles, and more.

## Sales

If you are interested in our products and services or want to cooperate with us, please contact us via mail or phone:

* **web:** www.aedifion.com
* **mail:** info (at) aedifion.com
* **phone:** +49 221 986 507 70

## Support

If you are already an aedifion customer and require (technical) support, please contact us via mail or phone:

* **mail:** support (at) aedifion.com
* **phone:** +49 221 986 507 70