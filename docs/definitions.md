
SWOP is a stateful message based protocol, i.e., it exchanges messages of certain types and format in a certain order. The expected messages and order of messages are defined by different high level flows.

Currently, SWOP defines messages and flows for writing setpoints and schedules. 

## Setpoints
Setpoints are immediate, one-shot, best-effort, low-overhead, acknowledged or not write operations.
An acknowledged setpoint has three states:

* `requested` - the issuer has sent the request for the setpoint write to the receiver. An unacknowledged setpoint will remain in this state.
* `written` - the receiver has acknowledged the successful write of this setpoint.
* `failed` - the receiver has acknowledged an error during the write of this setpoint or the write operation has already failed on the issuer side.


## Schedules

!!! warning "Work in progress!"
	Specification of schedules in version 0.1 is still work in progress and due to change.


Schedules are stateful, acknowledged, robust, and modifiable. They comprise multiple setpoints and are more robust and powerful than single setpoint writes.
States and transitions of schedules
The lifecycle of a schedule is defined by five possible states:

* A new schedules is always created in state `initialized`.
* When the schedule is syntactically correct and the specified project and data points exists the schedule will transition into state `active` after a few seconds.
* When a (critical) error occurs at any time, the schedule transitions into state `failed` where it has reached end-of-life (EoL).
* When a user cancels a schedule, it first transitions into `stopping` where it awaits acknowledgement from the edge device and cannot be modified anymore.
* When the schedule has been completely executed or successfully stopped by the user, it transitions into `terminated` where it has reached EoL.

### Structure and execution of schedules

A schedule  is formally defined as a series of setpoints, i.e.,
$$
S = (s_1, ..., s_n) = ((id_1, t_1, v_1), ..., (id_n, t_n, v_n))
$$

where $s_1,...,s_n$ are setpoints and each single setpoint $s_i$ within is defined by a unique id $id_i$, a start time $t_i$ and its value $v_i$.
The setpoints of a schedule are executed strictly in order of time. If two setpoints have the same start time, their ordering in $S$ determines which is executed first. With $t_1 \leq ... \leq t_n$ wlog., a schedule thus executes as follows:

* The schedule starts with execution of $s_1$ at time $t_1$ where $v_1$ is written.
* Setpoint $s_i$ is written at time $t_i$ and overwrites the previous value $v_{i-1}$ with $v_i$.
* If and setpoint $s_i$ lies in the past, i.e., $S$ is created after $t_i$, $s_i$ is exectued and written immediately.
* The schedule ends with execution of $s_n$ (we refer to this as the stop event) at time $t_n$ where $v_n$ is written.

!!! info "State after termination of schedules"

	The value written by the last setpoint of a schedule, i.e., the *stop event*, remains active in the building automation network until it is overwritten by another setpoint, another schedule, the user or other control algorithms.