## Writing to multiple automation protocols

In larger setups the edge device must talk to different automation components through different protocols, e.g., HVAC devices speak BACnet while switches are controlled through KNX and lightning is integrated through the DALI protocol.
SWOP protocol assumes that the [message field](messages.md) `datapoint` uniquely identifies the target datpoint among all existing automation networks.

On the rare ocassion that datapoints on two different automation networks are named the same it is up to the SWOP implementation to disambiguate.
It is the recommendation to add the `x-automation_protocol` field for this purpose.
`x-automation_protocol` should be of type object and should contain field `identifier` according to Table 1.
Other information, e.g., version of the standard used, is optional (see Example 1).

<center>

| Protocol | Identifier |
|----------|-------------------------|
| [BACnet](http://www.bacnet.org/) | bacnet |
| [DALI](https://www.digitalilluminationinterface.org/) | dali |
| [KNX](https://www.knx.org/) | knx |
| [LON](https://www.lonmark.org/) | lon |
| [Modbus](http://www.modbus.org/) | modbus |
| ... | ... |

**Table 1:** Protocol identifiers.</center>

```json
"x-automation_protocol: {
	'protocol': 'bacnet',
	'version': 'ISO 16484-5'
}
```
<center>**Example 1:** The `x-automation_protocol` field.</center>
