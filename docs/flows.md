We distinguish three main flow types:

1. [Setpoint flows](#setpoint-flows) for single setpoints.
2. [Schedule flows](#schedule-flows) for pre-defined schedules.
3. [Controls App flows](#controls-app-flows) for messages exchanged directly between a control application and the edge device.

## Setpoint Flows

### Write Setpoint Flow

The setpoint flow defines the type and order of messages for writing a single setpoint.

The usual flow without any errors proceeds as follows (square brackets mark optional steps).

1. The user posts the parameters for the new setpoint to the API, i.e., the issuer in SWOP.
1. The issuer validates the request, optionally creates internal state for this operation, then requests the setpoint write by sending a `NEWSPT` (new setpoint) message to the edge device, the receiver in SWOP.
1. The edge device receives and validates the `NEWSPT` message, then writes the setpoint.
1. If the setpoint must be acked (`acknowledge` flag), the edge device acknowledges success or error and additional state to the issuer in an `ACKSPT` message.
1. The issuer receives the optional `ACKSPT` and updates its internal state of this setpoint operation accordingly. 

If the receiver does not require and acknowledgement, Steps 4 and 5 are omitted.

#### Handling lost NewSetpoint messages

The receiver may be temporarily unreachable, or, more generally, the `NEWSPT` message may be lost in transit, e.g., when a non-direct transport such as MQTT is used between issuer and receiver. 
To handle this case, the issuer may choose to repeat its `NEWSPT` message after an appropriate timeout. 
It is, however, left to the application logic whether the write operation should be retried in this way or aborted altogether. 
Note that this is thus not a message or object field but must be a parameter of the API call (which is outside these protocol specifications).

#### Handling lost AcknowledgeSetpoint messages

The issuer may be temporarily unreachable, or, more generally, the `ACKSPT` message may be lost in transit, e.g., when a non-direct transport such as MQTT is used between issuer and receiver. 
The receiver knows its acknowledgement has been lost if it receives a duplicate `NEWSPT` message from the issuer.
It then reacts by sending a copy of the lost `ACKSPT` to the issuer.

## Schedule Flows

### Create Schedule Flow

The Create Schedule Flow defines the type and order of messages for creating a new schedule and initializing it on the edge device. 

The usual flow without any errors proceeds as follows.

1. The user posts the parameters for the new schedule to the API, i.e., the issuer in SWOP.
1. The issuer validates the request, creates internal state for this operation, then initiates the create schedule flow by sending a `NEWSCHD` (new schedule) message to the edge device, the receiver in SWOP.
1. The edge device receives and validates the `NEWSCHD` message, then initiates the schedule.
1. The edge device acknowledges success or error and additional state to the issuer in an `ACKSCHD` message.
1. The issuer receives the acknowledgement and updates its internal state of this schedule operation accordingly. 

### Update Schedule Flow

The Update Schedule Flow defines the type and order of messages for updating an active schedule. 

The usual flow without any errors proceeds as follows.

1. The user posts the reference of the target schedule and the parameters for the update to the issuer.
1. The issuer validates the request, updates internal state for this operation, then initiates the update schedule flow by sending a `UPSCHD` (update schedule) message to the receiver.
1. The edge device receives and validates the `UPSCHD` message, then updates the schedule.
1. The edge device acknowledges success or error and additional state to the issuer in an `ACKSCHD` message.
1. The issuer receives the acknowledgement and may again update its internal state of this schedule operation accordingly.

### Acknowledge Schedule Flow

The Acknowledge Schedule Flow defines the type and order of messages for acknowledging progress of the execution of a schedule from the receiver towards the issuer of the schedule. 
It is left to the SWOP implementation to decide which aspects of the execution of a schedule are acknowledged from the receiver to the issuer. 
We recommend to acknowledge success or failure of the execution of each setpoint defined in the schedule.
 
The usual flow without any errors proceeds as follows.

1. An event occurs on the receiver side, e.g., a setpoint is written.
1. The receiver acknowledges success or error and additional state about the occurred event to the issuer in an `ACKSCHD` message.
1. The issuer receives the acknowledgement and may again update its internal state of this schedule operation accordingly.

### Delete Schedule Flow

The Delete Schedule Flow defines the type and order of messages for stopping an active schedule and deleting it on the receiver site. The schedule is marked as stopped on the issuer side but it's state should be kept for auditing purposes.  

The usual flow without any errors proceeds as follows.

1. The user posts the reference of the schedule to delete to the issuer in SWOP.
2. The issuer validates the request, marks the schedule for deletion in the internal state, then initiates the delete schedule flow by sending a `DELSCHD` (delete schedule) message to the receiver.
3. The edge device receives and validates the `DELSCHD` message, then stops and deletes the schedule.
4. The edge device acknowledges success or error and additional state to the issuer in an `ACKSCHD` message.
5. The issuer receives the acknowledgement and may again update its internal state of this schedule operation accordingly.

## Controls App Flows

Currently, the Controls App Flow is based on an MQTT pipeline hosted by aedifion, i.e., an MQTT broker with pre-defined topics.

### Upsert Controls Flow

The Upsert Controls Flow defines the type and order of messages for creating and updating a controls app and initializing it on the edge device.

The usual flow without any errors proceeds as follows.

1. The user sends an `UPSRTCTRL` message via MQTT, i.e., the issuer in SWOP.
2. The edge device receives and validates the `UPSRTCTRL` message and stores reset values and further connectivity-related values internally.
3. The edge device acknowledges success or error to the issuer in an `ACKUPSRTCTRL` message.
4. The issuer receives the `ACKUPSRTCTRL`, updates its internal state of this controls operation accordingly and starts to run a controls app.
5. Sending of new setpoints as an output of a controls app is then based on the [Setpoint Flow](#setpoint-flows).

If the receiver does not require an acknowledgement, Step 4 is omitted.

### Reset Controls Flow

The Reset Controls Flow defines the type and order of messages for writing reset values of a controls app to the BAS.

The usual flow without any errors proceeds as follows.

1. The user sends a `RESETCTRL` message via MQTT, i.e., the issuer in SWOP.
2. The edge device receives and validates the `RESETCTRL` message and stores reset values and further connectivity related values internally.
3. The edge device acknowledges success or error to the issuer in an `ACKRESETCTRL` message.
4. The issuer receives the `ACKRESETCTRL` and updates its internal state of this controls operation accordingly.

If the receiver does not require an acknowledgement, Step 4 is omitted.

### Delete Controls Flow

The Delete Controls Flow defines the type and order of messages for deleting a controls app.

The usual flow without any errors proceeds as follows.

1. The user sends a `DELCTRL` message via MQTT, i.e., the issuer in SWOP. 
2. The edge device receives and validates the `DELCTRL` message and deletes the corresponding controls app data.
3. The edge device acknowledges success or error to the issuer in an `ACKDELCTRL` message.
4. The issuer receives the `ACKDELCTRL` and updates its internal state of this controls operation accordingly.

If the receiver does not require an acknowledgement, Step 4 is omitted.
