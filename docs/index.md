# Welcome

Welcome to the specification and documentation of the SWOP protocol.
The [current version of SWOP is 0.2](definitions.md).

## What is SWOP?

SWOP is a recursive acronym for *SWOP Writing Operation Protocol*. 
Regarding its purpose and goals, we may also call it 

* Simple Writing Operation Protocol,
* Standard Writing Operation Protocol,
* Safe Writing Operation Protocol.

SWOP is a lightweight JSON-based protocol for writing to building automation systems. 
Writing actively changes the control of the building, permanently or temporarily, by, e.g., 

* temporarily overwriting a setpoint, 
* adding a control strategy for weekend mode operations, 
* removing a suboptimal control schedule, ...

Actively intervening in building control is serious business. 
Thus, SWOP's primary maxim is correctness. 
All design goals and choices of SWOP, e.g., intuition, simplicity, etc., are made to promote this maxim.

## Why do I need SWOP?
SWOP defines an abstract interface between the issuer of a write operation (e.g., the user through an API) and the receiver of this operation, e.g., a special device connected to a building automation network. Thereby, SWOP decouples the issuer and receiver of a write operation.

This is important for different reasons:

Decoupling
:   Through SWOP's decoupling of issuer and receiver, we can pull control logic from local constrained SCADA systems and deploy it in remote systems, e.g., on a central cloud platform. 
	This has many advantages in terms of controllability, maintainability, expressivity, integration.


Integration and collaboration
:   SWOP provides a simple common language between issuer and receiver of a write operation allowing implementations, devices, and platforms of different vendors to talk to each other.

Abstraction 
:   SWOP is deliberately agnostic of the underlying automation bus and technology. 
	Thus, control algorithms (that issue write operations) can be developed and deployed independently of the underlying automation technology. 
	This fosters generalization, portability and reduces duplication of work.
	
## Scenario & Glossary

SWOP assumes and applies to the following simple scenario:

* We wish to write to a building, plant, or district, referred to as *target facility*.
* Write operations are initiated by humans, machines, or algorithms through a standard interface referred to as the API. 
  The API is the *issuer* of a write operation. 
  In the most general case, the API should be globally available, e.g., hosted on a central server outside of the target facility's closed local networks.
* An *edge device* is located at the target facility and connected to the Internet. 
  The edge device is the *receiver* of the write operation. 
  It is responsible for actually performing the write operation and, for this purpose, is connected to the target facility's automation network.
* Typically, the edge device is separated from the Internet through a firewall that filters traffic and prevents connection attempts from the outside. 
  Thus, we assume that communication with the edge device cannot be initiated from the outside, e.g., from the API server, but the edge device can initiate communication with the API server or other servers on the Internet.

Note that the outlined scenario is general enough to cover and apply to a multitude of control scenarios, e.g.,

* *cloud-based control*, where control logic and APIs are located in the cloud completely separated from the edge device which acts as a pure proxy for writing to the automation bus,
* *remote control*, where control logic is located on a user's PC and operations are written through a local or cloud-based API,
* *on-premise control*, where control logic is hosted on the edge device which acts as both the issuer and receiver of write operations for multiple devices and the automation network,
* *agent-based control*, where control logic is distributed over multiple edge devices that communicate with each other via SWOP.

