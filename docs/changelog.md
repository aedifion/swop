# Changelog

## Version 0.2

* Specifications for controls app related messages.

## Version 0.1

First released version of SWOP.

* Specifications for setpoint writing.
* Specifications for schedule writing. 