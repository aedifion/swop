docs-build:
	mkdocs build --clean -d build/docs

docs-serve:
	mkdocs serve

python-tests:
	PYTHONPATH=python/:. python3 python/swop/test/test_swop_protocol.py
